package ch.hearc.kaboom.powerup;

import ch.hearc.kaboom.GameScreen;
import ch.hearc.kaboom.Player;
import com.badlogic.gdx.graphics.Texture;

/**
 * Invincibility power up specialized class
 * This Power Up gives the player invincibility for a short instant.
 */
public class PUInvincibility extends PowerUp{

    /**
     * Invincibility power up constructor
     * @param parent Parent Screen
     */
    public PUInvincibility(GameScreen parent){
        super(parent);
        this.sprite = new Texture("powerups/pow_invincibility.png");
    }

    /**
     * Invincibility power up constructor
     * @param x X Position
     * @param y Y Position
     * @param parent Parent Screen
     */
    public PUInvincibility(int x, int y, GameScreen parent){
        super(x, y, parent);
        this.sprite = new Texture("powerups/pow_invincibility.png");
    }

    /**
     * Called when the player picks the power up
     * @param p Player
     */
    @Override
    public void pick(Player p){
        if(!almostDead) {
            if(p.getCooldownFireMax() > 0.2F)
                p.setCooldownInvincibilityMax(5);
            setDead();
        }
    }


}
