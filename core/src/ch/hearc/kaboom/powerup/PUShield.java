package ch.hearc.kaboom.powerup;

import ch.hearc.kaboom.GameScreen;
import ch.hearc.kaboom.Player;
import com.badlogic.gdx.graphics.Texture;

/**
 * Shield Power Up specialized class
 * This power up gives the player a shield that protects him of an attack
 */
public class PUShield extends PowerUp {

    /**
     * Shield power up constructor
     * @param parent Parent Screen
     */
    public PUShield(GameScreen parent){
        super(parent);
        this.sprite = new Texture("powerups/pow_shield.png");
    }

    /**
     * Shield power up constructor
     * @param x X position
     * @param y Y position
     * @param parent Parent Screen
     */
    public PUShield(int x, int y, GameScreen parent){
        super(x, y, parent);
        this.sprite = new Texture("powerups/pow_shield.png");
    }

    /**
     * Is called when the player picks the power up
     * @param p Player
     */
    @Override
    public void pick(Player p){
        if(!almostDead) {
            if(p.getLife() != 2)
                p.heal(1);
            setDead();
        }
    }

}
