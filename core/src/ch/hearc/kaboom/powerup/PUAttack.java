package ch.hearc.kaboom.powerup;

import ch.hearc.kaboom.GameScreen;
import ch.hearc.kaboom.Player;
import com.badlogic.gdx.graphics.Texture;

/**
 * Attack PowerUp specialized class
 * This Power Up reduces the cooldown between two projectiles
 */
public class PUAttack extends PowerUp{

    /**
     * Attack Power Up constructor
     * @param parent Parent Screen
     */
    public PUAttack(GameScreen parent){
        super(parent);
        this.sprite = new Texture("powerups/pow_attack.png");
    }

    /**
     * Attack Power Up Constructor
     * @param x X Position
     * @param y Y Position
     * @param parent Parent Screen
     */
    public PUAttack(int x, int y, GameScreen parent){
        super(x, y, parent);
        this.sprite = new Texture("powerups/pow_attack.png");
    }

    /**
     * Called when the Player picks the power up
     * @param p Player
     */
    @Override
    public void pick(Player p){
        if(!almostDead) {
            if(p.getCooldownFireMax() > 0.2F)
                p.changeCooldownFireMax(-0.1F);
            setDead();
        }
    }

}
