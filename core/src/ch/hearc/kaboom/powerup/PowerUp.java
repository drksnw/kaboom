package ch.hearc.kaboom.powerup;

import ch.hearc.kaboom.Entity;
import ch.hearc.kaboom.GameScreen;
import ch.hearc.kaboom.Player;

/**
 * General PowerUp class
 */
public class PowerUp extends Entity {

    /**
     * Power Up constructor
     * @param parent Parent Screen
     */
    public PowerUp(GameScreen parent){
        super(parent);
        speed = 100;
    }

    /**
     * Power Up Constructor
     * @param x X position
     * @param y Y position
     * @param parent Parent Screen
     */
    public PowerUp(int x, int y, GameScreen parent){
        super(x, y, parent);
        speed = 100;
    }

    /**
     * Update method. Called once a frame.
     * @param delta Elaped time between two frames
     */
    @Override
    public void update(float delta){
        moveX(-delta);
        if(x + sprite.getWidth() < 0){
            alive = false;
        }
    }

    /**
     * Called when the player picks the PowerUp
     * @param p Player
     */
    public void pick(Player p){

    }

}
