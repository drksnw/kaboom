package ch.hearc.kaboom.powerup;

import ch.hearc.kaboom.GameScreen;
import ch.hearc.kaboom.Player;
import com.badlogic.gdx.graphics.Texture;

/**
 * Weapon power up specialized class
 * This Power Up gives the player an enhanced weapon.
 * It shots 3 projectiles instead of one.
 */
public class PUWeapon extends PowerUp {

    /**
     * Weapon powerup constructor
     * @param parent Parent Screen
     */
    public PUWeapon(GameScreen parent){
        super(parent);
        this.sprite = new Texture("powerups/pow_weapon.png");
    }

    /**
     * Weapon power up constructor
     * @param x X position
     * @param y Y position
     * @param parent Parent Screen
     */
    public PUWeapon(int x, int y, GameScreen parent){
        super(x, y, parent);
        this.sprite = new Texture("powerups/pow_weapon.png");
    }

    /**
     * Called when the player picks a power up.
     * @param p Player
     */
    @Override
    public void pick(Player p){
        if(!almostDead) {
            p.setTypeAttack(2);
            setDead();
        }
    }
}
