package ch.hearc.kaboom.powerup;

import ch.hearc.kaboom.GameScreen;
import ch.hearc.kaboom.Player;
import com.badlogic.gdx.graphics.Texture;

/**
 * Speed power up specialized class
 * This power up raises the player's speed
 */
public class PUSpeed extends PowerUp{

    /**
     * Speed power up constructor
     * @param parent Parent Screen
     */
    public PUSpeed(GameScreen parent){
        super(parent);
        this.sprite = new Texture("powerups/pow_speed.png");
    }

    /**
     * Speed power up constructor
     * @param x X position
     * @param y Y position
     * @param parent
     */
    public PUSpeed(int x, int y, GameScreen parent){
        super(x, y, parent);
        this.sprite = new Texture("powerups/pow_speed.png");
    }

    /**
     * Called when the player picks the power up
     * @param p Player
     */
    @Override
    public void pick(Player p){
        if(!almostDead) {
            if(p.getSpeed() < 300)
                p.changeSpeed(25);
            setDead();
        }
    }


}
