package ch.hearc.kaboom;

import ch.hearc.kaboom.enemies.Enemy;
import ch.hearc.kaboom.projectiles.Projectile;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

/**
 * Entity class. Define all game objects.
 */
public class Entity {

    protected int x,y,speed,life = 1,team;
    protected Texture sprite;
    protected boolean alive = true;
    protected boolean almostDead = false;
    protected GameScreen parent;
    protected double explosionIndex = 0;

    /**
     * Base Entity constructor
     * @param parent Parent Screen
     */
    public Entity(GameScreen parent) {
        this.parent = parent;
    }

    /**
     * Entity Constructor
     * @param x X position
     * @param y Y position
     * @param parent Parent Screen
     */
    public Entity(int x, int y, GameScreen parent){
        this.x = x;
        this.y = y;
        this.parent = parent;
    }

    /**
     * Moves the entity on X axis.
     * @param delta Time elapsed since last call (Normal behaviour)
     */
    public void moveX(float delta){
        x += speed*delta;
    }

    /**
     * Moves the entity on Y axis.
     * @param delta Time elasped since last call.
     */
    public void moveY(float delta){
        y += speed*delta;
    }

    /**
     * Moves the entity to a specific point on Y axis.
     * @param y Point where to move the entity
     */
    public void moveToY(int y){
        this.y = y;
    }

    /**
     * Draws the entity on screen
     * @param sb Game's SpriteBatch
     */
    public void draw(SpriteBatch sb){
        if(!almostDead) {
            sb.begin();
            sb.draw(sprite, x, y);
            sb.end();
        } else {
            if(this instanceof Enemy) {
                if(explosionIndex == 0){
                    parent.enemySnd.play();
                }
                sb.begin();
                sb.draw(parent.explosion[(int) explosionIndex], x-75, y-75, 150, 150);
                sb.end();
                explosionIndex += 0.5;
                if (explosionIndex >= parent.explosion.length) {
                    alive = false;
                }
            } else {
                alive = false;
            }
        }
    }

    /**
     * Fire a projectile from this entity
     * @return The fired projectile
     */
    public Entity fire(){
        Projectile p = new Projectile(x+10, y+sprite.getHeight()/2-7,team,parent,1);
        return p;
    }

    /**
     * Gets this entity's hitbox
     * @return Rectangle containing the sprite
     */
    public Rectangle getBoundingRect(){
        return new Rectangle(x, y, sprite.getWidth(), sprite.getHeight());
    }

    /**
     * Check if the entity is alive
     * @return boolean
     */
    public boolean isAlive(){
        return alive;
    }

    /**
     * Gets the entity's team
     * @return Entity's team
     */
    public int getTeam() {
        return team;
    }

    /**
     * Gets the entity's life.
     * @return Entity's life
     */
    public int getLife() {
        return life;
    }

    /**
     * Gets the entity's speed
     * @return Entity's speed
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * Gets the entity's X position
     * @return Entity's X position
     */
    public float getX() { return x; }

    /**
     * Gets the entity's Y position
     * @return Entity's Y position
     */
    public float getY() { return y; }

    /**
     * Kills the entity
     */
    public void setDead(){
        almostDead = true;
    }

    /**
     * Changes this entity's speed
     * @param delta Difference of speed
     */
    public void changeSpeed(int delta) {
        this.speed += delta;
    }

    /**
     * Is called when the Entity is hurt
     */
    public void hurt(){
        life--;
        if(life <= 0)
            setDead();
    }

    /**
     * Heals the entity
     * @param hp Number of life points to give
     */
    public void heal(int hp){
        life += hp;
    }

    /**
     * Checks if the entity collides with another entity
     * @param e The other entity
     * @return Boolean
     */
    public boolean collidesWith(Entity e){
        if(e.getBoundingRect().overlaps(getBoundingRect())){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update method. Called once a frame. Have to be specialized
     * @param delta Time elapsed between two frames
     */
    public void update(float delta) {

    }
}
