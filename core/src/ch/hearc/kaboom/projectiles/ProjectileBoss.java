package ch.hearc.kaboom.projectiles;

import ch.hearc.kaboom.GameScreen;
import com.badlogic.gdx.graphics.Texture;

/**
 * Specialized class for Boss' projectiles
 */
public class ProjectileBoss extends Projectile {

    /**
     * Boss' projectile constructor
     * @param x Initial X position
     * @param y Initial Y position
     * @param team Projectile's team (Always set to Enemy here.)
     * @param parent Parent screen
     * @param type Defines the projectile's behaviour
     */
    public ProjectileBoss(int x, int y, int team, GameScreen parent, int type){
        super(x,y,team,parent,type);
        this.speed = 250;
        this.sprite = new Texture("bitmaps/projectileBoss.png");
    }

    /**
     * Update method. Called once per frame.
     * @param delta Time elapsed between two frames
     */
    @Override
    public void update(float delta)
    {
        x += speed*-delta;
        if(type == 2) {
            totalDelta += delta;
            moveToY((int) (baseY + Math.sin(totalDelta * 4) * 50));
        } else if(type == 3) {
            totalDelta += delta;
            moveToY((int) (baseY - Math.sin(totalDelta * 4) * 50));
        }

        if(x + sprite.getWidth() <= 0){
            alive = false;
        }
    }
}
