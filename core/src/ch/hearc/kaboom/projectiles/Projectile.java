package ch.hearc.kaboom.projectiles;

import ch.hearc.kaboom.Entity;
import ch.hearc.kaboom.GameScreen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Gdx2DPixmap;

/**
 * Base Projectile class
 */
public class Projectile extends Entity {

    protected int type, baseY;
    protected float totalDelta;

    /**
     * Projectile constructor
     * @param x Initial X position
     * @param y Initial Y position
     * @param team Projectile's team (Fired by player or enemy)
     * @param parent Parent Screen
     * @param type Defines the Projectile's behaviour
     */
    public Projectile(int x, int y, int team, GameScreen parent, int type){
        super(x, y,parent);
        this.team = team;
        this.speed = 250;
        this.sprite = new Texture("bitmaps/projectile.png");
        this.type = type;
        baseY = y;
    }

    /**
     * Update method. Called once a frame.
     * @param delta Time elapsed between two frames
     */
    @Override
    public void update(float delta)
    {
        x += speed*delta;
        if(type == 2) {
            totalDelta += delta;
            moveToY((int) (baseY + Math.sin(totalDelta * 4) * 50));
        } else if(type == 3) {
            totalDelta += delta;
            moveToY((int) (baseY - Math.sin(totalDelta * 4) * 50));
        }
        if(x > Gdx.graphics.getWidth()){
            alive = false;
        }
    }
}
