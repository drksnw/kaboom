package ch.hearc.kaboom.projectiles;

import ch.hearc.kaboom.GameScreen;
import com.badlogic.gdx.graphics.Texture;

/**
 * Specialized class for Level 3 Boss' projectile
 */
public class ProjectileBoss3 extends Projectile {

    private int deplY;
    private float realY;

    /**
     * Level 3 boss' projectile constructor.
     * @param x Initial X position
     * @param y Initial Y position
     * @param team Projectile's team (always Enemy here)
     * @param parent Parent Screen
     * @param type Defines projectile's behaviour
     */
    public ProjectileBoss3(int x, int y, int team, GameScreen parent, int type){
        super(x,y,team,parent,type);
        this.speed = 250;
        this.sprite = new Texture("bitmaps/projectileBoss3.png");
    }

    /**
     * Level 3 boss' projectile oonstructor
     * @param x Initial X position
     * @param y Initial Y position
     * @param deplY Number of pixel per second (Y-velocity)
     * @param team Projectile's team (always Enemy here)
     * @param parent Parent Screen
     */
    public ProjectileBoss3(int x, int y, int deplY, int team, GameScreen parent){
        super(x,y,team,parent,6);
        this.speed = 250;
        this.sprite = new Texture("bitmaps/projectileBoss3.png");
        this.deplY = deplY;
        realY = y;
    }

    /**
     * Update method. Called once per frame
     * @param delta Time elapsed between two frames
     */
    @Override
    public void update(float delta)
    {
        x += speed*-delta;
        if(type == 2) {
            totalDelta += delta;
            moveToY((int) (baseY + Math.sin(totalDelta * 4) * 50));
        } else if(type == 3) {
            totalDelta += delta;
            moveToY((int) (baseY - Math.sin(totalDelta * 4) * 50));
        } else if(type == 4) {
            totalDelta += delta;
            moveToY((int) (240 - Math.sin(totalDelta * 4) * 240));
        } else if(type == 5) {
            totalDelta += delta;
            moveToY((int) (240 + Math.sin(totalDelta * 4) * 240));
        } else if(type == 6){
            realY += deplY*delta;
            y = (int)realY;
        }

        if(x + sprite.getWidth() <= 0){
            alive = false;
        }
    }
}
