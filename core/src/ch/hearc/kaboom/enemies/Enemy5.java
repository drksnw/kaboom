package ch.hearc.kaboom.enemies;

import ch.hearc.kaboom.GameScreen;
import com.badlogic.gdx.graphics.Texture;

/**
 * Enemy 5 Specialized class
 */
public class Enemy5 extends Enemy{

    /**
     * Enemy 5 Constructor
     * @param parent Parent Screen
     */
    public Enemy5(GameScreen parent){
        super(parent);
        speed = 300;
        this.sprite = new Texture("bitmaps/enemy5.png");
    }

    /**
     * Enemy 5 Constructor
     * @param y Initial Y position
     * @param parent Parent Screen
     */
    public Enemy5(int y, GameScreen parent){
        super(y,parent);
        speed = 300;
        this.sprite = new Texture("bitmaps/enemy5.png");
    }

    /**
     * Enemy 5 Constructor
     * @param x Initial X position
     * @param y Initial Y position
     * @param parent Parent Screen
     */
    public Enemy5(int x, int y, GameScreen parent){
        super(x,y,parent);
        speed = 300;
        this.sprite = new Texture("bitmaps/enemy5.png");
    }

    /**
     * Update method. Called once a frame
     * @param delta Time elapsed between two frames
     */
    public void update(float delta){
        super.update(delta);
        moveX(-delta);
    }
}
