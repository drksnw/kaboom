package ch.hearc.kaboom.enemies;

import ch.hearc.kaboom.Entity;
import ch.hearc.kaboom.GameScreen;
import ch.hearc.kaboom.powerup.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Basic Enemy handling
 */
public class Enemy extends Entity {

    protected int baseY;
    protected float totalDelta;

    /**
     * Enemy constructor
     * @param parent Parent Screen
     */
    public Enemy(GameScreen parent){
        super(parent);
        x = Gdx.graphics.getWidth() - 15;
        y = Gdx.graphics.getHeight()/2;
        baseY = y;
        team = 2;
    }

    /**
     * Enemy constructor
     * @param y Initial Y position
     * @param parent Parent Screen
     */
    public Enemy(int y, GameScreen parent){
        super(Gdx.graphics.getWidth() - 15,y,parent);
        baseY = y;
        team = 2;
    }

    /**
     * Enemy constructor
     * @param x Initial X position
     * @param y Initial Y position
     * @param parent Parent Screen
     */
    public Enemy(int x, int y, GameScreen parent){
        super(x,y,parent);
        team = 2;
        baseY = y;
    }

    /**
     * Update method. Called once a frame.
     * @param delta Time elapsed between two frames
     */
    public void update(float delta){
        if(x + sprite.getWidth() < 0){
            alive = false;
        }
    }

    /**
     * Sets the enemy dead.
     * Handles Power-Up apparition
     */
    @Override
    public void setDead() {
        super.setDead();

        if (ThreadLocalRandom.current().nextInt(1, 15 + 1) == 1)
            switch(ThreadLocalRandom.current().nextInt(1, 5 + 1)){
                case 1:
                    parent.addEntity(new PUShield(x, y, parent));
                    break;
                case 2:
                    parent.addEntity(new PUAttack(x, y, parent));
                    break;
                case 3:
                    parent.addEntity(new PUInvincibility(x, y, parent));
                    break;
                case 4:
                    parent.addEntity(new PUSpeed(x, y, parent));
                    break;
                case 5:
                    parent.addEntity(new PUWeapon(x, y, parent));
                    break;

            }
    }

}
