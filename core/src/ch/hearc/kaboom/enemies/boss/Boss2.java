package ch.hearc.kaboom.enemies.boss;

import ch.hearc.kaboom.Entity;
import ch.hearc.kaboom.GameScreen;
import ch.hearc.kaboom.enemies.Enemy5;
import ch.hearc.kaboom.projectiles.ProjectileBoss;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Manages the level 2 boss
 */
public class Boss2 extends Boss{

    /**
     * Boss 2 Constructor
     * @param shapeRenderer Game's Shape Renderer
     * @param parent Parent screen
     */
    public Boss2(ShapeRenderer shapeRenderer, GameScreen parent){
        super(shapeRenderer, parent);
        x = Gdx.graphics.getWidth() + 50;
        y = Gdx.graphics.getHeight()/2;
        speed = 120;
        this.sprite = new Texture("bitmaps/boss2.png");
        life = 200;
        lifeMax = life;
        nextLevel = 2;
    }

    /**
     * Update method. Called once a frame
     * @param delta Time elapsed between two frames
     */
    @Override
    public void update(float delta){
        super.update(delta);

        if(cooldownFire > 0) {
            cooldownFire -= delta;
        }else{
            parent.addEntity(fire());
            cooldownFire = 1F;
        }

    }

    /**
     * Boss firing method.
     * @return Entity fired (Can be a Projectile, or an Enemy)
     */
    @Override
    public Entity fire(){
        Entity p = null;
        switch(typeProj) {
            case 1:
                p = new Enemy5(x + 10, y + sprite.getHeight() / 2 - 7,  parent);
                parent.addEntity(new Enemy5(x + 10, y + sprite.getHeight() / 2 - 57,  parent));
                parent.addEntity(new Enemy5(x + 10, y + sprite.getHeight() / 2 + 43,  parent));
                parent.addEntity(new Enemy5(x + 10, y + sprite.getHeight() / 2 - 107,  parent));
                parent.addEntity(new Enemy5(x + 10, y + sprite.getHeight() / 2 + 93,  parent));
                typeProj = 2;
                break;
            case 2:
                p = new ProjectileBoss(x + 10, y + sprite.getHeight() / 2 - 7 + 70, team, parent, 2);
                parent.addEntity(new ProjectileBoss(x + 10, y + sprite.getHeight() / 2 - 7 - 70, team, parent, 3));
                typeProj = 1;
                break;
        }

        return p;
    }
}
