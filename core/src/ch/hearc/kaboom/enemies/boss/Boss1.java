package ch.hearc.kaboom.enemies.boss;
import ch.hearc.kaboom.GameScreen;
import ch.hearc.kaboom.projectiles.Projectile;
import ch.hearc.kaboom.projectiles.ProjectileBoss;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Manages the boss for the level 1
 */
public class Boss1 extends Boss{
    /**
     * Boss1's constructor
     * @param shapeRenderer Game's ShapeRenderer
     * @param parent Parent screen
     */
    public Boss1(ShapeRenderer shapeRenderer, GameScreen parent){
        super(shapeRenderer, parent);
        x = Gdx.graphics.getWidth() + 50;
        y = Gdx.graphics.getHeight()/2;
        speed = 100;
        this.sprite = new Texture("bitmaps/boss1.png");
        life = 150;
        lifeMax = life;
        nextLevel = 1;
    }

    /**
     * Update method, called once a frame
     * @param delta Time elapsed between two frames
     */
    @Override
    public void update(float delta){
        super.update(delta);

        if(cooldownFire > 0) {
            cooldownFire -= delta;
        }else{
            parent.addEntity(fire());
            cooldownFire = 0.7F;
        }

    }

    /**
     * Creates a new Projectile fired by the boss
     * @return Fired Projectile
     */
    @Override
    public Projectile fire(){
        ProjectileBoss p = null;
        switch(typeProj) {
            case 1:
                p = new ProjectileBoss(x + 10, y + sprite.getHeight() / 2 - 7, team, parent, 1);
                typeProj = 2;
                break;
            case 2:
                p = new ProjectileBoss(x + 10, y + sprite.getHeight() / 2 - 7 + 70, team, parent, 2);
                typeProj = 3;
                break;
            case 3:
                p = new ProjectileBoss(x + 10, y + sprite.getHeight() / 2 - 7 - 70, team, parent, 3);
                typeProj = 1;
                break;
        }

        return p;
    }
}
