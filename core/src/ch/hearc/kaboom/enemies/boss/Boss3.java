package ch.hearc.kaboom.enemies.boss;

import ch.hearc.kaboom.Entity;
import ch.hearc.kaboom.GameScreen;
import ch.hearc.kaboom.enemies.Enemy;
import ch.hearc.kaboom.enemies.Enemy1;
import ch.hearc.kaboom.enemies.Enemy4;
import ch.hearc.kaboom.projectiles.ProjectileBoss3;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.ArrayList;

/**
 * Manages the Boss 3
 */
public class Boss3 extends Boss{

    private int nbTirs = 0,typeProjAttack1 = 2, posYAttack5 = 16, typeProjAttack6 = 4, deplYAttack8=0, signeAttack8, state = 1;
    private ArrayList<Expl> listExpl = new ArrayList<Expl>();

    /**
     * Boss 3 constructor
     * @param shapeRenderer Game's Shape Renderer
     * @param parent Parent screen
     */
    public Boss3(ShapeRenderer shapeRenderer, GameScreen parent){
        super(shapeRenderer, parent);
        x = Gdx.graphics.getWidth() + 50;
        y = 0;
        speed = 120;
        this.sprite = new Texture("bitmaps/boss3Hidden.png");
        life = 300;
        lifeMax = life;
        typeProj = 0;
    }

    /**
     * Function that draws the boss
     * @param sb The game's SpriteBatch
     */
    public void draw(SpriteBatch sb){
        sb.begin();
        sb.draw(sprite, x, y);
        sb.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.rect(10,5,Gdx.graphics.getWidth() - 20, 5);
        shapeRenderer.setColor(Color.YELLOW);
        shapeRenderer.rect(10,5,(Gdx.graphics.getWidth() - 20)*life/lifeMax, 5);
        shapeRenderer.end();

        for(int i = 0; i < listExpl.size(); i++){

            sb.begin();
            sb.draw(parent.explosion[listExpl.get(i).index], listExpl.get(i).x, listExpl.get(i).y, 150, 150);
            sb.end();

            listExpl.get(i).index++;
            if (listExpl.get(i).index >= parent.explosion.length) {
                listExpl.remove(i);
                i--;
            }
        }
    }

    /**
     * Update method. Called once a frame
     * @param delta Time elapsed between two frames
     */
    @Override
    public void update(float delta){
        if(almostDead){
            if(y > -sprite.getHeight()) {
                moveY(-delta / 1.5F);
                if (cooldownFire > 0) {
                    cooldownFire -= delta;
                } else {
                    int explX = (int) (Math.random() * sprite.getWidth()) + x;
                    int explY = (int) (Math.random() * sprite.getHeight()) + y;
                    listExpl.add(new Expl(explX, explY));
                    parent.enemySnd.play();
                    cooldownFire = 0.05F;
                }
            }else{
                alive = false;
                parent.endingLevel();
                parent.setLevel(nextLevel);
            }
        }else {
            if (x >= Gdx.graphics.getWidth() - sprite.getWidth() + 15) {
                moveX(-delta);
            }

            if (cooldownFire > 0) {
                cooldownFire -= delta;
            } else {
                fire();
            }
        }
    }

    /**
     * Fire method
     * Multiple pattern available. Choosed randomly.
     * @return Entity fired.
     */
    @Override
    public Entity fire(){
        Entity p = null;
        switch(typeProj) {
            case 0:
                parent.addEntity(new ProjectileBoss3(x + 10, (int)(Math.random()*15*32), team, parent, 1));
                cooldownFire = 0.6F;
                break;
            case 1:

                if(nbTirs < 40) {
                    for (int i = 48; i < 4 * 32; i += 32) {
                        parent.addEntity(new ProjectileBoss3(x + 10, i, team, parent, typeProjAttack1));
                    }
                    for (int i = 16 + 11*32; i < 14 * 32; i += 32) {
                        parent.addEntity(new ProjectileBoss3(x + 10, i, team, parent, typeProjAttack1));
                    }
                    typeProjAttack1 = (typeProjAttack1+1)%2+2;
                    cooldownFire = 0.02F;
                    nbTirs++;
                }else{
                    changeTypeProj();
                }
                break;
            case 2:
                if(nbTirs < 40) {
                    for (int i = 16 + 5*32; i < 10 * 32; i += 32) {
                        parent.addEntity(new ProjectileBoss3(x + 10, i, team, parent, typeProjAttack1));
                    }
                    typeProjAttack1 = (typeProjAttack1+1)%2+2;
                    cooldownFire = 0.02F;
                    nbTirs++;
                }else{
                    changeTypeProj();
                }
                break;
            case 3:
                    for (int i = 48; i < 4 * 32; i += 32) {
                        parent.addEntity(new Enemy1(x + 10, i, parent));
                    }
                    for (int i = 16 + 11*32; i < 14 * 32; i += 32) {
                        parent.addEntity(new Enemy1(x + 10, i, parent));
                    }
                    changeTypeProj();

                break;
            case 4:
                    for (int i = 16 + 5*32; i < 10 * 32; i += 32) {
                        parent.addEntity(new Enemy4(x + 10, i, parent));
                    }
                    changeTypeProj();
                break;
            case 5:
                if(nbTirs < 50) {
                    parent.addEntity(new ProjectileBoss3(x + 10, 240, team, parent, typeProjAttack6));
                    cooldownFire = 0.01F;
                    nbTirs++;
                    typeProjAttack6 = (typeProjAttack6+1)%2+4;
                }else {
                    changeTypeProj();
                }
                break;
            case 6:
                if(nbTirs < 50) {
                    parent.addEntity(new ProjectileBoss3(x + 10, posYAttack5, team, parent, 1));
                    nbTirs++;
                    posYAttack5 = (int)(Math.random()*15*32);
                    cooldownFire = 0.15F;
                }else {
                    changeTypeProj();
                }
                break;
            case 7:
                if(nbTirs < 100) {
                    parent.addEntity(new ProjectileBoss3(x + 10, 240, team, parent, typeProjAttack6));
                    parent.addEntity(new ProjectileBoss3(x + 10, 240, team, parent, typeProjAttack6-2));
                    cooldownFire = 0.01F;
                    nbTirs++;
                    typeProjAttack6 = (typeProjAttack6+1)%2+4;
                }else{
                    changeTypeProj();
                }
                break;
            case 8:
                if(nbTirs == 0) signeAttack8 = (int)(Math.random()*2)*2-1;
                if(nbTirs < 40) {
                    for (int i = 48; i < 4 * 32; i += 32) {
                        parent.addEntity(new ProjectileBoss3(x + 10, i, team, parent, typeProjAttack1));
                    }
                    for (int i = 16 + 11*32; i < 14 * 32; i += 32) {
                        parent.addEntity(new ProjectileBoss3(x + 10, i, team, parent, typeProjAttack1));
                    }
                    typeProjAttack1 = (typeProjAttack1+1)%2+2;
                    cooldownFire = 0.02F;
                    nbTirs++;
                }else if(nbTirs < 100) {
                    parent.addEntity(new ProjectileBoss3(x + 10, 46+3*32, deplYAttack8, team, parent));
                    parent.addEntity(new ProjectileBoss3(x + 10, 11*32-44, deplYAttack8, team, parent));
                    cooldownFire = 0.02F;
                    deplYAttack8+=4*signeAttack8;
                    nbTirs++;
                }else if(nbTirs < 120) {
                    parent.addEntity(new ProjectileBoss3(x + 10, 46+3*32, deplYAttack8, team, parent));
                    parent.addEntity(new ProjectileBoss3(x + 10, 11*32-44, deplYAttack8, team, parent));
                    cooldownFire = 0.02F;
                    nbTirs++;
                }else if(nbTirs < 180) {
                    parent.addEntity(new ProjectileBoss3(x + 10, 46+3*32, deplYAttack8, team, parent));
                    parent.addEntity(new ProjectileBoss3(x + 10, 11*32-44, deplYAttack8, team, parent));
                    cooldownFire = 0.02F;
                    deplYAttack8-=4*signeAttack8;
                    nbTirs++;
                }else {
                    changeTypeProj();
                    deplYAttack8 = 0;
                }
                break;
            case 9:
                if(nbTirs == 0) signeAttack8 = (int)(Math.random()*2)*2-1;
                if(nbTirs < 40) {
                    for (int i = 16 + 5*32; i < 10 * 32; i += 32) {
                        parent.addEntity(new ProjectileBoss3(x + 10, i, team, parent, typeProjAttack1));
                    }
                    typeProjAttack1 = (typeProjAttack1+1)%2+2;
                    cooldownFire = 0.02F;
                    nbTirs++;
                }else if(nbTirs < 100) {
                    parent.addEntity(new ProjectileBoss3(x + 10, 46+3*32, -deplYAttack8, team, parent));
                    parent.addEntity(new ProjectileBoss3(x + 10, 11*32-44, deplYAttack8, team, parent));
                    cooldownFire = 0.02F;
                    deplYAttack8+=4;
                    nbTirs++;
                }else if(nbTirs < 101) {
                    cooldownFire = 0.02F;
                    deplYAttack8+=1000;
                    nbTirs++;
                }else if(nbTirs < 131) {
                    parent.addEntity(new ProjectileBoss3(x + 50, 46+3*32, -deplYAttack8, team, parent));
                    parent.addEntity(new ProjectileBoss3(x + 50, 11*32-44, deplYAttack8, team, parent));
                    cooldownFire = 0.02F;
                    deplYAttack8-=40;
                    nbTirs++;
                }else {
                    changeTypeProj();
                }
                break;
            case 10:
                if(nbTirs < 50) {
                    parent.addEntity(new ProjectileBoss3(x + 10, 240, team, parent, typeProjAttack6));
                    parent.addEntity(new ProjectileBoss3(x + 10, 240, team, parent, typeProjAttack6-2));
                    cooldownFire = 0.01F;
                    nbTirs++;
                    typeProjAttack6 = (typeProjAttack6+1)%2+4;
                }else if(nbTirs < 70) {
                    parent.addEntity(new ProjectileBoss3(x + 10, 240, team, parent, typeProjAttack6));
                    cooldownFire = 0.01F;
                    nbTirs++;
                    typeProjAttack6 = (typeProjAttack6+1)%2+4;
                }else{
                    changeTypeProj();
                }
                break;
            case 11:
                if(nbTirs == 0) signeAttack8 = (int)(Math.random()*2)*2-1;
                if(nbTirs < 40) {
                    for (int i = 48; i < 4 * 32; i += 32) {
                        parent.addEntity(new ProjectileBoss3(x + 10, i, team, parent, typeProjAttack1));
                    }
                    for (int i = 16 + 11*32; i < 14 * 32; i += 32) {
                        parent.addEntity(new ProjectileBoss3(x + 10, i, team, parent, typeProjAttack1));
                    }
                    typeProjAttack1 = (typeProjAttack1+1)%2+2;
                    cooldownFire = 0.02F;
                    nbTirs++;
                }else if(nbTirs < 100) {
                    parent.addEntity(new ProjectileBoss3(x + 10, 46+3*32, deplYAttack8, team, parent));
                    parent.addEntity(new ProjectileBoss3(x + 10, 11*32-44, deplYAttack8, team, parent));
                    cooldownFire = 0.02F;
                    deplYAttack8+=4*signeAttack8;
                    nbTirs++;
                }else if(nbTirs < 120) {
                    parent.addEntity(new ProjectileBoss3(x + 10, 46+3*32, deplYAttack8, team, parent));
                    parent.addEntity(new ProjectileBoss3(x + 10, 11*32-44, deplYAttack8, team, parent));
                    cooldownFire = 0.02F;
                    nbTirs++;
                }else if(nbTirs < 170) {
                    parent.addEntity(new ProjectileBoss3(x + 10, 46+3*32, deplYAttack8, team, parent));
                    parent.addEntity(new ProjectileBoss3(x + 10, 11*32-44, deplYAttack8, team, parent));
                    cooldownFire = 0.02F;
                    deplYAttack8-=4*signeAttack8;
                    nbTirs++;
                }else if(nbTirs < 180) {
                    parent.addEntity(new ProjectileBoss3(x + 10, 46+3*32, deplYAttack8, team, parent));
                    parent.addEntity(new ProjectileBoss3(x + 10, 11*32-44, deplYAttack8, team, parent));
                    cooldownFire = 0.02F;
                    nbTirs++;
                }else {
                    changeTypeProj();
                    deplYAttack8 = 0;
                }
                break;
            case 12:
                if(nbTirs == 0) signeAttack8 = (int)(Math.random()*2)*2-1;
                if(nbTirs < 40) {
                    for (int i = 16 + 5*32; i < 10 * 32; i += 32) {
                        parent.addEntity(new ProjectileBoss3(x + 10, i, team, parent, typeProjAttack1));
                    }
                    typeProjAttack1 = (typeProjAttack1+1)%2+2;
                    cooldownFire = 0.02F;
                    nbTirs++;
                }else if(nbTirs < 100) {
                    parent.addEntity(new ProjectileBoss3(x + 10, 46+3*32, -deplYAttack8, team, parent));
                    parent.addEntity(new ProjectileBoss3(x + 10, 11*32-44, deplYAttack8, team, parent));
                    cooldownFire = 0.02F;
                    deplYAttack8+=4;
                    nbTirs++;
                }else if(nbTirs < 101) {
                    cooldownFire = 0.02F;
                    deplYAttack8+=1000;
                    nbTirs++;
                }else if(nbTirs < 130) {
                    parent.addEntity(new ProjectileBoss3(x + 50, 46+3*32, -deplYAttack8, team, parent));
                    parent.addEntity(new ProjectileBoss3(x + 50, 11*32-44, deplYAttack8, team, parent));
                    cooldownFire = 0.02F;
                    deplYAttack8-=40;
                    nbTirs++;
                }else {
                    changeTypeProj();
                }
                break;
        }

        return p;
    }

    /**
     * Changes the attack pattern
     */
    private void changeTypeProj(){
        nbTirs = 0;
        switch(state){
            case 2:
                typeProj = (int)(Math.random()*6)+1;
                cooldownFire = 0.75F;
                break;
            case 3:
                typeProj = (int)(Math.random()*3)+7;
                cooldownFire = 0.75F;
                break;
            case 4:
                typeProj = (int)(Math.random()*3)+10;
                cooldownFire = 0.1F;
                break;
        }
    }

    /**
     * Is called when the boss is hurt.
     * Handles multi-phase boss.
     */
    public void hurt(){
        life--;
        if(state == 1 && life <= 1){
            state++;
            changeTypeProj();

            parent.enemySnd.play();
            parent.getBgm().stop();
            parent.setBgm(parent.getBgms().get(parent.getBgms().size()-1));
            parent.getBgm().loop(0.3f);
            this.sprite = new Texture("bitmaps/boss3.png");
            life = 2500;
            lifeMax = life;
        }else if (state == 2 && life < 1500){
            state++;
            this.sprite = new Texture("bitmaps/boss3Red.png");
        }else if (state == 3 && life < 500){
            state++;
            this.sprite = new Texture("bitmaps/boss3Final.png");
        }else if(state == 4 && life <= 0){
            almostDead = true;
            this.sprite = new Texture("bitmaps/boss3Hidden.png");
        }
    }
}

/**
 * Class for the explosions at the end of the boss
 */
class Expl{
    public int x,y,index = 0;
    public Expl(int x, int y){
        this.x = x;
        this.y = y;
    }
}