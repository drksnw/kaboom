package ch.hearc.kaboom.enemies.boss;

import ch.hearc.kaboom.GameScreen;
import ch.hearc.kaboom.MenuScreen;
import ch.hearc.kaboom.enemies.Enemy;
import ch.hearc.kaboom.projectiles.Projectile;
import ch.hearc.kaboom.projectiles.ProjectileBoss;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Class Boss
 * Manage a boss
 */
public class Boss extends Enemy {

    protected boolean top = true;
    protected int lifeMax, typeProj = 1, nextLevel = 0;
    protected float cooldownFire;
    protected ShapeRenderer shapeRenderer;


    /**
     * Boss constructor
     * @param shapeRenderer The game's ShapeRenderer
     * @param parent The parent screen
     */
    public Boss(ShapeRenderer shapeRenderer, GameScreen parent){
        super(parent);
        x = Gdx.graphics.getWidth() + 50;
        y = Gdx.graphics.getHeight()/2;
        this.shapeRenderer = shapeRenderer;
    }

    /**
     * Draws the boss
     * @param sb The game's SpriteBatch
     */
    @Override
    public void draw(SpriteBatch sb){
        super.draw(sb);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.rect(10,5,Gdx.graphics.getWidth() - 20, 5);
        shapeRenderer.setColor(Color.YELLOW);
        shapeRenderer.rect(10,5,(Gdx.graphics.getWidth() - 20)*life/lifeMax, 5);
        shapeRenderer.end();
    }

    /**
     * Boss' update method. Called once per frame
     * @param delta Time elapsed between two frames
     */
    @Override
    public void update(float delta) {
        if (x >= Gdx.graphics.getWidth() - sprite.getWidth() - 10) {
            moveX(-delta);
        } else if (!top) {
            moveY(-delta);
            if (y <= 0) {
                top = true;
            }
        } else {
            moveY(delta);
            if (y + sprite.getHeight() >= Gdx.graphics.getHeight()) {
                top = false;
            }
        }
    }

    /**
     * Called when the boss is dead
     */
    @Override
    public void setDead(){
        super.setDead();
        parent.endingLevel();
        parent.setLevel(nextLevel);
    }

}
