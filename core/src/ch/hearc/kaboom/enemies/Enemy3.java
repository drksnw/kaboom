package ch.hearc.kaboom.enemies;

import ch.hearc.kaboom.GameScreen;
import ch.hearc.kaboom.projectiles.Projectile;
import ch.hearc.kaboom.projectiles.ProjectileBoss;
import com.badlogic.gdx.graphics.Texture;

/**
 * Enemy 3 Specialized class
 */
public class Enemy3 extends Enemy {

    private float cooldownFire = 2F;

    /**
     * Enemy 3 constructor
     * @param parent Parent Screen
     */
    public Enemy3(GameScreen parent){
        super(parent);
        speed = 60;
        life = 3;
        this.sprite = new Texture("bitmaps/enemy3.png");
    }

    /**
     * Enemy 3 Constructor
     * @param y Initial Y position
     * @param parent Parent Screen
     */
    public Enemy3(int y, GameScreen parent){
        super(y,parent);
        speed = 60;
        life = 3;
        this.sprite = new Texture("bitmaps/enemy3.png");
    }

    /**
     * Enemy 3 Constructor
     * @param x Initial X position
     * @param y Initial Y position
     * @param parent Parent Screen
     */
    public Enemy3(int x, int y, GameScreen parent){
        super(x,y,parent);
        speed = 60;
        life = 3;
        this.sprite = new Texture("bitmaps/enemy3.png");
    }

    /**
     * Update method. Called once a frame
     * @param delta Time elapsed between two frames
     */
    public void update(float delta){
        super.update(delta);
        totalDelta += delta;
        moveX(-delta);

        if(cooldownFire > 0)
            cooldownFire-=delta;
        else{
            cooldownFire = 2F;
            parent.addEntity(fire());
        }
    }

    /**
     * Firing method
     * @return Fired projectile
     */
    @Override
    public Projectile fire(){
        return new ProjectileBoss(x + 10, y + sprite.getHeight() / 2 - 7, team, parent, 1);
    }
}
