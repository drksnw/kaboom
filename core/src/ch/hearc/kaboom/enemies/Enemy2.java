package ch.hearc.kaboom.enemies;

import ch.hearc.kaboom.GameScreen;
import com.badlogic.gdx.graphics.Texture;

/**
 * Enemy 2 Specialized class
 */
public class Enemy2 extends Enemy{

    private float cooldownY = 0.5F;
    private int stateDepl = 0;
    private int targetY;

    /**
     * Enemy 2 Constructor
     * @param parent Parent Screen
     */
    public Enemy2(GameScreen parent){
        super(parent);
        speed = 100;
        this.sprite = new Texture("bitmaps/enemy2.png");
    }

    /**
     * Enemy 2 Constructor
     * @param y Initial Y position
     * @param parent Parent Screen
     */
    public Enemy2(int y, GameScreen parent){
        super(y,parent);
        speed = 100;
        this.sprite = new Texture("bitmaps/enemy2.png");
    }

    /**
     * Enemy 2 Constructor
     * @param x Initial X position
     * @param y Initial Y position
     * @param parent Parent Screen
     */
    public Enemy2(int x, int y, GameScreen parent){
        super(x,y,parent);
        speed = 100;
        this.sprite = new Texture("bitmaps/enemy2.png");
    }

    /**
     * Update method. Called once a frame
     * @param delta Time elapsed between two frames
     */
    public void update(float delta){
        super.update(delta);
        totalDelta += delta;
        moveX(-delta);

        if(cooldownY > 0)
            cooldownY -= delta;
        else if(stateDepl == 0){
            targetY =(int)(Math.random()*160)-80+baseY;
            if(targetY > y)
                stateDepl = 1;
            else
                stateDepl = -1;
        }else if(stateDepl == 1){
            if(targetY > y)
                moveY(delta*2);
            else {
                stateDepl = 0;
                cooldownY = 0.5F;
            }
        }else if(stateDepl == -1){
            if(targetY < y)
                moveY(-delta*2);
            else {
                stateDepl = 0;
                cooldownY = 0.5F;
            }
        }

    }

}
