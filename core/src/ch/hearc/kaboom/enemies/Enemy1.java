package ch.hearc.kaboom.enemies;

import ch.hearc.kaboom.GameScreen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

/**
 * Enemy 1 Specialized class
 */
public class Enemy1 extends Enemy {

    /**
     * Enemy 1 Constructor
     * @param parent Parent Screen
     */
    public Enemy1(GameScreen parent){
        super(parent);
        speed = 100;
        this.sprite = new Texture("bitmaps/enemy1.png");
    }

    /**
     * Enemy 1 Constructor
     * @param y Initial Y position
     * @param parent Parent Screen
     */
    public Enemy1(int y, GameScreen parent){
        super(y,parent);
        speed = 100;
        this.sprite = new Texture("bitmaps/enemy1.png");
    }

    /**
     * Enemy 1 Constructor
     * @param x Initial X position
     * @param y Initial Y position
     * @param parent Parent Screen
     */
    public Enemy1(int x, int y, GameScreen parent){
        super(x,y,parent);
        speed = 100;
        this.sprite = new Texture("bitmaps/enemy1.png");
    }

    /**
     * Update method. Called once a frame.
     * @param delta Time elapsed between two frames
     */
    public void update(float delta){
        super.update(delta);
        totalDelta += delta;
        moveX(-delta);
        moveToY((int)(baseY + Math.sin(totalDelta*4)*50));
    }
}
