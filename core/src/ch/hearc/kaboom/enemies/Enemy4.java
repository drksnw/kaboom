package ch.hearc.kaboom.enemies;

import ch.hearc.kaboom.GameScreen;
import com.badlogic.gdx.graphics.Texture;

/**
 * Enemy 4 Specialized class
 */
public class Enemy4 extends Enemy{

    private int typeDepl = 1;

    /**
     * Enemy 4 Constructor
     * @param parent Parent Screen
     */
    public Enemy4(GameScreen parent){
        super(parent);
        speed = 120;
        this.sprite = new Texture("bitmaps/enemy4.png");
    }

    /**
     * Enemy 4 Constructor
     * @param y Initial Y position
     * @param parent Parent Screen
     */
    public Enemy4(int y, GameScreen parent){
        super(y,parent);
        speed = 120;
        this.sprite = new Texture("bitmaps/enemy4.png");
    }

    /**
     * Enemy 4 Constructor
     * @param x Initial X position
     * @param y Initial Y position
     * @param parent Parent Screen
     * @param typeDepl Deplacement type
     */
    public Enemy4(int x, int y, GameScreen parent, int typeDepl){
        super(y,parent);
        speed = 120;
        this.sprite = new Texture("bitmaps/enemy4.png");
        this.typeDepl = typeDepl;
    }

    /**
     * Enemy 4 constructor
     * @param x Initial X position
     * @param y Initial Y position
     * @param parent Parent Screen
     */
    public Enemy4(int x, int y, GameScreen parent){
        super(x,y,parent);
        speed = 120;
        this.sprite = new Texture("bitmaps/enemy4.png");
    }

    /**
     * Update method. Called once a frame
     * @param delta Time elapsed between two frames
     */
    public void update(float delta){
        super.update(delta);
        totalDelta += delta;
        moveX(-delta);
        if(typeDepl == 1)
            moveToY((int)(baseY + Math.sin(totalDelta*4)*75));
        else if(typeDepl == 2)
            moveToY((int)(baseY - Math.sin(totalDelta*4)*75));
    }
}
