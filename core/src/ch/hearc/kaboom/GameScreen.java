package ch.hearc.kaboom;

import ch.hearc.kaboom.enemies.boss.Boss;
import ch.hearc.kaboom.enemies.boss.Boss1;
import ch.hearc.kaboom.enemies.boss.Boss2;
import ch.hearc.kaboom.enemies.*;
import ch.hearc.kaboom.enemies.boss.Boss3;
import ch.hearc.kaboom.powerup.PowerUp;
import ch.hearc.kaboom.projectiles.Projectile;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

import java.util.ArrayList;
import java.util.List;

/**
 * The actual game's screen
 */
public class GameScreen implements Screen {
    private Kaboom game;
    private Player p;
    private List<Entity> entities = new ArrayList<Entity>();
    private List<Entity> newEntities = new ArrayList<Entity>();
    private TiledMap map;
    private int mapWidth;
    private OrthogonalTiledMapRenderer renderer;
    private boolean isBossSpawned = false;
    private boolean cheat = false;

    /**
     * Array containing the maps paths.
     */
    private static final String[] maps ={"maps/map1.tmx","maps/map2.tmx", "maps/map3.tmx"};

    /**
     * Array containing the musics paths.
     */
    private static final String[] musics ={"bgm/bgm1.wav","bgm/bgm2.ogg","bgm/bgm3.ogg","bgm/finalBattle.ogg"};
    private List<Boss> listBoss = new ArrayList<Boss>();
    private static int level = 0;
    private boolean endLevel = false;

    public Texture[] explosion;
    public Sound enemySnd;
    private Sound playerShot;

    private Sound bgm;
    private static List<Sound> bgms = new ArrayList<Sound>();

    /**
     * Gets the actual playing music
     * @return Actual playing music
     */
    public Sound getBgm() {
        return bgm;
    }

    /**
     * Sets the playing music
     * @param bgm New music
     */
    public void setBgm(Sound bgm) {
        this.bgm = bgm;
    }

    /**
     * Gets the collection of music
     * @return Music's collection
     */
    public List<Sound> getBgms() {
        return bgms;
    }

    /**
     * Gets the main game class
     * @deprecated Never used.
     * @return Main game class
     */
    public Kaboom getGame() {
        return game;
    }

    /**
     * Inits all the sounds.
     * Slows the startup process, but level changes is instant.
     */
    public static void initSound(){
        if(bgms.isEmpty()){
            for(String s : musics){
                bgms.add(Gdx.audio.newSound(Gdx.files.internal(s)));
            }
        }
    }

    /**
     * GameScreen constructor
     * @param game Kaboom's main class
     */
    public GameScreen(final Kaboom game) {
        this.game = game;
        explosion = new Texture[10];
        for(int i = 0; i < 10; i++){
            explosion[i] = new Texture("explosions/"+i+".png");
        }
        enemySnd = Gdx.audio.newSound(Gdx.files.internal("sfx/enemy_explode.wav"));
        bgm = bgms.get(level);
        playerShot = Gdx.audio.newSound(Gdx.files.internal("sfx/player_shoot.wav"));
        bgm.loop(0.3f);
    }

    /**
     * Is called when the screen is shown
     */
    @Override
    public void show() {
        p = new Player(this);
        listBoss.add(new Boss1(game.getShapeRenderer(), this));
        listBoss.add(new Boss2(game.getShapeRenderer(), this));
        listBoss.add(new Boss3(game.getShapeRenderer(), this));

        map = new TmxMapLoader().load(maps[level]);
        mapWidth = map.getProperties().get("width", Integer.class) * map.getProperties().get("tilewidth", Integer.class);

        renderer = new OrthogonalTiledMapRenderer(map, 1 / 1f);
    }

    /**
     * Is called once a frame, draws all entities.
     * @param delta Time elapsed since last frame
     */
    @Override
    public void render(float delta) {
        update(delta);

        renderer.setView(game.getCam());
        renderer.render();

        if (game.getCam().position.x + Gdx.graphics.getWidth() / 2 < mapWidth -10)
            game.getCam().translate(80 * delta, 0);

        else if (!isBossSpawned)
            spawnBoss();

        p.draw(game.getBatch());

        for (Entity e : entities) {
            e.draw(game.getBatch());
        }


        game.getCam().update();
    }

    /**
     * Is called once a frame, contains the game loop.
     * @param delta Time elapsed since last frame
     */
    public void update(float delta) {
        if(endLevel){
            if(p.getX() < Gdx.graphics.getWidth())
                p.moveX(delta);
            else{
                game.setScreen(new MenuScreen(game, "Next level"));
                bgm.stop();
            }

        }else {
            p.update(delta);

            if (!p.isAlive()) {
                game.setScreen(new MenuScreen(game, "Restart"));
                bgm.stop();
            }

            //Déplacements du joueur
            if (Gdx.input.isKeyPressed(Input.Keys.W)) {
                p.moveY(delta);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.S)) {
                p.moveY(-delta);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.D)) {
                p.moveX(delta);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.A)) {
                p.moveX(-delta);
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.K))
                cheat = !cheat;

            if (Gdx.input.isKeyPressed(Input.Keys.SPACE) && p.canFire()) {
                entities.add(p.fire());
                playerShot.play(0.2f);
            }
        }

            for (Entity e : entities) {
                for (Entity f : entities) {
                    if (f instanceof Projectile) {
                        if (!(e instanceof Projectile) && e.getTeam() != f.getTeam() && !(e instanceof PowerUp)) {
                            if (e.collidesWith(f)) {
                                e.hurt();
                                f.setDead();
                            }
                        }
                    }
                }
                if (e instanceof Projectile && p.collidesWith(e) && p.getTeam() != e.getTeam() && e.isAlive() && !p.isInvincible()) {
                    p.hurt();
                    e.setDead();
                }
                if (e.collidesWith(p) && e instanceof Enemy && !p.isInvincible()) {
                    p.hurt();
                }
                if (e.collidesWith(p) && e instanceof PowerUp) {
                    ((PowerUp) e).pick(p);
                }
                e.update(delta);
            }

            for (int i = 0; i < entities.size(); i++) {
                if (!entities.get(i).isAlive()) {
                    entities.remove(i);
                    i--;
                }
            }

            int x = (int) (game.getCam().position.x + Gdx.graphics.getWidth() / 2) / 32;
            for (int y = map.getProperties().get("height", Integer.class); y >= 0; y--) {
                MapLayer layer = map.getLayers().get("enemies");
                if (layer instanceof TiledMapTileLayer) {
                    TiledMapTileLayer.Cell cell = ((TiledMapTileLayer) layer).getCell(x, y);
                    if (cell != null) {
                        switch (cell.getTile().getId()) {
                            case 1:
                                entities.add(new Enemy1(y * 32, this));
                                ((TiledMapTileLayer) layer).setCell(x, y, null);
                                break;
                            case 2:
                                entities.add(new Enemy2(y * 32, this));
                                ((TiledMapTileLayer) layer).setCell(x, y, null);
                                break;
                            case 3:
                                entities.add(new Enemy3(y * 32, this));
                                ((TiledMapTileLayer) layer).setCell(x, y, null);
                                break;
                            case 4:
                                entities.add(new Enemy4(y * 32, this));
                                ((TiledMapTileLayer) layer).setCell(x, y, null);
                                break;
                            case 5:
                                entities.add(new Enemy5(y * 32, this));
                                ((TiledMapTileLayer) layer).setCell(x, y, null);
                                break;
                            case 6:
                                entities.add(new Enemy6(y * 32, this));
                                ((TiledMapTileLayer) layer).setCell(x, y, null);
                                break;
                        }
                    }
                }
            }

            for (Entity entity : newEntities) {
                entities.add(entity);
            }
            newEntities.clear();

    }

    /**
     * Is called when the screen is resized
     * @deprecated The screen's size is fixed
     */
    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    /**
     * Spawns the boss
     */
    private void spawnBoss() {
        isBossSpawned = true;
        entities.add(listBoss.get(level));
    }

    /**
     * Adds new entity to the screen
     * @param entity New entity
     */
    public void addEntity(Entity entity){
        newEntities.add(entity);
    }

    /**
     * Check if the player is cheating
     * @return Boolean
     */
    public boolean isCheat() {
        return cheat;
    }

    /**
     * Gets the player object
     * @return Player object
     */
    public Player getPlayer() {
        return p;
    }

    /**
     * Sets the actual level
     * @param level Level number
     */
    public void setLevel(int level){ GameScreen.level = level;}

    /**
     * Sets the end level
     */
    public void endingLevel(){ this.endLevel = true;}

    /**
     * Check if it's end level
     * @return Boolean
     */
    public boolean isEndLevel(){ return endLevel;}
}