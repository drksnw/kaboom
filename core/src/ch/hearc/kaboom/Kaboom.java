package ch.hearc.kaboom;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Main game class
 */
public class Kaboom extends Game {
	private SpriteBatch batch;
	private OrthographicCamera cam;
    private BitmapFont font;
	private ShapeRenderer shapeRenderer;

	/**
	 * Method called when the game is launched
	 */
	@Override
	public void create () {
		batch = new SpriteBatch();
		cam = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		cam.setToOrtho(false);
        font = new BitmapFont();
		shapeRenderer = new ShapeRenderer();
		GameScreen.initSound();
		this.setScreen(new MenuScreen(this, "New game"));
	}

	/**
	 * Gets the game's Sprite Batch
	 * @return Game's Sprite Batch
     */
	public SpriteBatch getBatch(){
		return batch;
	}

	/**
	 * Gets the game's camera
	 * @return The game's camera
     */
	public OrthographicCamera getCam(){
		return cam;
	}

	/**
	 * Gets the game's shape renderer
	 * @return The game's shape renderer
     */
	public ShapeRenderer getShapeRenderer() {
		return shapeRenderer;
	}

	/**
	 * Is called once a frame
	 */
	@Override
	public void render () {
		super.render();
	}
}
