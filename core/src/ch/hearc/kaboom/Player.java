package ch.hearc.kaboom;

import ch.hearc.kaboom.projectiles.Projectile;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Player class
 */
public class Player extends Entity{

    protected Texture spriteShield;
    protected Texture spriteInvincibility;
    protected float cooldownInvincibility = 0;
    protected float cooldownFire = 0;
    protected float cooldownFireMax = 0.5F;
    protected int typeAttack = 1;

    /**
     * Player constructor
     * @param parent Parent Screen
     */
    public Player(GameScreen parent){
        super(parent);
        x = 15;
        y = Gdx.graphics.getHeight()/2;
        speed = 200;
        this.sprite = new Texture("bitmaps/ship.png");
        this.spriteShield = new Texture("bitmaps/shipShield.png");
        this.spriteInvincibility = new Texture("bitmaps/shipInvincibility.png");
        team = 1;
    }

    /**
     * Player's constructor
     * @param x X position
     * @param y Y position
     * @param parent Parent Screen
     */
    public Player(int x, int y, GameScreen parent){
        super(x,y,parent);
        speed = 200;
        this.sprite = new Texture("bitmaps/ship.png");
        this.spriteShield = new Texture("bitmaps/shipShield.png");
        this.spriteInvincibility = new Texture("bitmaps/shipInvincibility.png");
        team = 1;
    }

    /**
     * Draws the player on the screen
     * @param sb Game's SpriteBatch
     */
    public void draw(SpriteBatch sb) {
        super.draw(sb);

        if(isInvincible()){
            sb.begin();
            sb.draw(spriteInvincibility, x-10, y-2);
            sb.end();
        }else if(life > 1){
            sb.begin();
            sb.draw(spriteShield, x-10, y-2);
            sb.end();
        }
    }

    /**
     * Is called when the player's hurt
     */
    @Override
    public void hurt() {
        super.hurt();

        cooldownInvincibility = 1;
    }

    /**
     * Update method. Called once a frame.
     * @param delta Time elapsed between two frames
     */
    @Override
    public void update(float delta) {
        cooldownInvincibility -= delta;
        cooldownFire -= delta;
    }

    /**
     * Checks if the player is invincible
     * @return Boolean
     */
    public boolean isInvincible() {
        return cooldownInvincibility > 0;
    }

    /**
     * Check if the player can shoot
     * @return boolean
     */
    public boolean canFire() {
        return cooldownFire <= 0;
    }

    /**
     * Moves the player on X-axis
     * @param delta Time elapsed since last call (Normal behaviour)
     */
    @Override
    public void moveX(float delta){
        super.moveX(delta);
        if(x < 0){
            x = 0;
        }else if(x + sprite.getWidth() > Gdx.graphics.getWidth() && !parent.isEndLevel()){
            x = Gdx.graphics.getWidth() - sprite.getWidth();
        }
    }

    /**
     * Moves the player on Y-axis
     * @param delta Time elasped since last call.
     */
    @Override
    public void moveY(float delta){
        super.moveY(delta);
        if(y < 0){
            y = 0;
        }else if(y + sprite.getHeight() > Gdx.graphics.getHeight()){
            y = Gdx.graphics.getHeight() - sprite.getHeight();
        }
    }

    /**
     * Fire a projectile
     * @return Fired projectile
     */
    @Override
    public Projectile fire(){
        Projectile p = new Projectile(x+10, y+sprite.getHeight()/2-7,team,parent,1);
        cooldownFire = cooldownFireMax;
        if(parent.isCheat())
            cooldownFire /= 5;

        if(typeAttack == 2){
            parent.addEntity(new Projectile(x+10, y+sprite.getHeight()/2-7,team,parent,2));
            parent.addEntity(new Projectile(x+10, y+sprite.getHeight()/2-7,team,parent,3));
            cooldownFire *= 2;
        }
        return p;
    }

    /**
     * Gets the cooldown value
     * @return Cooldown value
     */
    public float getCooldownFireMax() {
        return cooldownFireMax;
    }

    /**
     * Changes the cooldown value
     * @param delta Cooldown to add
     */
    public void changeCooldownFireMax(float delta){
        cooldownFireMax += delta;
    }

    /**
     * Sets the invincibility cooldown
     * @param cooldown New invincibility cooldown
     */
    public void setCooldownInvincibilityMax(float cooldown){
        cooldownInvincibility = cooldown;
    }

    /**
     * Change the attack's type
     * @param typeAttack New attack type
     */
    public void setTypeAttack(int typeAttack) {
        this.typeAttack = typeAttack;
    }
}
